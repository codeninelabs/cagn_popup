import 'package:cagn_core/core.dart';
import 'package:flutter/material.dart';

import 'position_aware.dart';

const kDefaultDialogSize = Size(250, 250);
const kDefaultDialogAlignment = Alignment.topRight;
const kDefaultDialogPivot = Alignment.topLeft;
const kDefaultDialogOffset = Offset.zero;
const kDefaultDisplaySpeed = 100;

/// Function used to display the Popup widget.
///
/// This function is used to display a popup using the specified parameters.
/// The parameters can be used to determine the size and screen placement of
/// the popup.
showCagnPopup(
  /// BuildContext to use with the popup.
  BuildContext context, {

  /// The required widget to be displayed in the popup
  required Widget child,

  /// Determines if the popup can be dismissed when the user taps outside the popup
  bool barrierDismissible = true,

  /// The position details as determined by the [PositionAwareWidget]
  required PositionDetails details,

  /// The geometry details of the popup [PopupProps]
  required PopupProps props,

  /// The background [Color] to use for the popup background. Although the background
  /// can be set on the child, this parameter will be used for the drawn arrow.
  Color? color, //for arrow once developed

  ///  defines the size of the popup shadow.
  final double? elevation = 0,

  /// The [Rect] to use when absolutely positioning the popup. This parameter
  /// supersedes the [pivot] and [alignment] parameters.
  Rect? absolutePosition,

  /// The speed in milliseconds to show and hide to popup.
  int? displaySpeed,

  /// The delay in milliseconds before the popup is shown.
  // int? displayDelay,

  /// The delay in milliseconds before the popup is shown.
  // Curve? displayCurve,

  /// Custom transition to use when displaying the popup. This allows for
  /// custom animations for the popup.
  Widget Function(BuildContext, Animation<double>, Animation<double>, Widget)?
      customTransitionBuilder,
}) {
  PopupProps _props = cagnPopupPropsHelper(context, props, details);
  Widget _child = Theme(
    data: ThemeData(
        cardTheme: CardTheme(
      elevation: elevation,
      color: Theme.of(context).cardColor,
    )),
    child: Card(
      child: child,
    ),
  );
  Positioned _positioned = Positioned.fill(child: _child);
  if (absolutePosition != null) {
    _positioned = Positioned.fromRect(rect: absolutePosition, child: _child);
  } else if (_props.pivot != null) {
    _positioned = _pivotedAt(
      details: details,
      child: _child,
      props: _props,
    );
  } else {
    _positioned = _alignmentFromScreen(
      context,
      props: _props,
      child: _child,
    );
  }

  return _showCagnDialog(
    context: context,
    child: Stack(children: [_positioned]),
    barrierDismissible: barrierDismissible,
    displaySpeed: displaySpeed ?? kDefaultDisplaySpeed,
    customTransitionBuilder: customTransitionBuilder,
  );
}

Positioned _alignmentFromScreen(
  BuildContext context, {
  required PopupProps props,
  required Widget child,
}) {
  final screenWidth = DeviceUtility.screenWidth;
  final screenHeight = DeviceUtility.screenHeight;
  Positioned _positioned = Positioned(
      left: 0 + props.offset.dx, top: 0 + props.offset.dy, child: child);
  double xCenter = (screenWidth - props.size.width) / 2;
  double yCenter = (screenHeight - props.size.height) / 2;
  if (props.alignment == Alignment.topRight) {
    _positioned = Positioned(
        right: 0 + props.offset.dx, top: 0 + props.offset.dy, child: child);
  } else if (props.alignment == Alignment.topCenter) {
    _positioned =
        Positioned(left: xCenter, top: 0 + props.offset.dy, child: child);
  } else if (props.alignment == Alignment.topLeft) {
    _positioned = Positioned(
        left: 0 + props.offset.dx, top: 0 + props.offset.dy, child: child);
  } else if (props.alignment == Alignment.centerLeft) {
    _positioned =
        Positioned(left: 0 + props.offset.dx, top: yCenter, child: child);
  } else if (props.alignment == Alignment.center) {
    _positioned = Positioned(left: xCenter, top: yCenter, child: child);
  } else if (props.alignment == Alignment.centerRight) {
    _positioned =
        Positioned(right: 0 + props.offset.dx, top: yCenter, child: child);
  } else if (props.alignment == Alignment.bottomLeft) {
    _positioned = Positioned(
        left: 0 + props.offset.dx, bottom: 0 + props.offset.dy, child: child);
  } else if (props.alignment == Alignment.bottomCenter) {
    _positioned =
        Positioned(left: xCenter, bottom: 0 + props.offset.dy, child: child);
  } else if (props.alignment == Alignment.bottomRight) {
    _positioned = Positioned(
        right: 0 + props.offset.dx, bottom: 0 + props.offset.dy, child: child);
  }
  return _positioned;
}

Positioned _pivotedAt({
  required PositionDetails details,
  required PopupProps props,
  required Widget child,
}) {
  if (props.pivot == Alignment.topRight) {
    return _positionedFrom(
      child: child,
      details: details,
      props: props,
      alignmentOffset: Offset(props.size.width, 0),
    );
  } else if (props.pivot == Alignment.topCenter) {
    return _positionedFrom(
      child: child,
      details: details,
      props: props,
      alignmentOffset: Offset(props.size.width / 2, 0),
    );
  } else if (props.pivot == Alignment.centerLeft) {
    return _positionedFrom(
      child: child,
      details: details,
      props: props,
      alignmentOffset: Offset(0, props.size.height / 2),
    );
  } else if (props.pivot == Alignment.center) {
    return _positionedFrom(
      child: child,
      details: details,
      props: props,
      alignmentOffset: Offset(props.size.width / 2, props.size.height / 2),
    );
  } else if (props.pivot == Alignment.centerRight) {
    return _positionedFrom(
      child: child,
      details: details,
      props: props,
      alignmentOffset: Offset(props.size.width, props.size.height / 2),
    );
  } else if (props.pivot == Alignment.bottomLeft) {
    return _positionedFrom(
      child: child,
      details: details,
      props: props,
      alignmentOffset: Offset(0, props.size.height),
    );
  } else if (props.pivot == Alignment.bottomCenter) {
    return _positionedFrom(
      child: child,
      details: details,
      props: props,
      alignmentOffset: Offset(props.size.width / 2, props.size.height),
    );
  } else if (props.pivot == Alignment.bottomRight) {
    return _positionedFrom(
      child: child,
      details: details,
      props: props,
      alignmentOffset: Offset(props.size.width, props.size.height),
    );
  } else {
    return _positionedFrom(
      child: child,
      details: details,
      props: props,
    );
  }
}

Positioned _positionedFrom({
  required PositionDetails details,
  required Widget child,
  required PopupProps props,
  Offset alignmentOffset: Offset.zero,
}) {
  Positioned _positioned = Positioned(
    left: details.position.dx + details.size.width + props.offset.dx,
    top: details.position.dy,
    child: child,
  );
  if (props.alignment == Alignment.topRight) {
    _positioned = Positioned(
      left: details.position.dx +
          details.size.width +
          props.offset.dx -
          alignmentOffset.dx,
      top: details.position.dy + props.offset.dy - alignmentOffset.dy,
      child: child,
    );
  } else if (props.alignment == Alignment.topCenter) {
    _positioned = Positioned(
      left: details.position.dx +
          (details.size.width / 2) +
          props.offset.dx -
          alignmentOffset.dx,
      top: details.position.dy + props.offset.dy - alignmentOffset.dy,
      child: child,
    );
  } else if (props.alignment == Alignment.topLeft) {
    _positioned = Positioned(
      left: (details.position.dx) + props.offset.dx - alignmentOffset.dx,
      top: details.position.dy + props.offset.dy - alignmentOffset.dy,
      child: child,
    );
  } else if (props.alignment == Alignment.centerLeft) {
    _positioned = Positioned(
      left: (details.position.dx) + props.offset.dx - alignmentOffset.dx,
      top: (details.position.dy + (details.size.height / 2)) +
          props.offset.dy -
          alignmentOffset.dy,
      child: child,
    );
  } else if (props.alignment == Alignment.center) {
    _positioned = Positioned(
      left: details.position.dx +
          (details.size.width / 2) +
          props.offset.dx -
          alignmentOffset.dx,
      top: (details.position.dy + (details.size.height / 2)) +
          props.offset.dy -
          alignmentOffset.dy,
      child: child,
    );
  } else if (props.alignment == Alignment.centerRight) {
    _positioned = Positioned(
      left: details.position.dx +
          details.size.width +
          props.offset.dx -
          alignmentOffset.dx,
      top: (details.position.dy + (details.size.height / 2)) +
          props.offset.dy -
          alignmentOffset.dy,
      child: child,
    );
  } else if (props.alignment == Alignment.bottomLeft) {
    _positioned = Positioned(
      left: (details.position.dx) + props.offset.dx - alignmentOffset.dx,
      top: (details.position.dy + details.size.height) +
          props.offset.dy -
          alignmentOffset.dy,
      child: child,
    );
  } else if (props.alignment == Alignment.bottomCenter) {
    _positioned = Positioned(
      left: details.position.dx +
          (details.size.width / 2) +
          props.offset.dx -
          alignmentOffset.dx,
      top: (details.position.dy + details.size.height) +
          props.offset.dy -
          alignmentOffset.dy,
      child: child,
    );
  } else if (props.alignment == Alignment.bottomRight) {
    _positioned = Positioned(
      left: details.position.dx +
          details.size.width +
          props.offset.dx -
          alignmentOffset.dx,
      top: (details.position.dy + details.size.height) +
          props.offset.dy -
          alignmentOffset.dy,
      child: child,
    );
  }
  return _positioned;
}

Future<T?> _showCagnDialog<T>({
  required BuildContext context,
  required Widget child,
  bool barrierDismissible = true,
  int displaySpeed: 200,
  Widget Function(BuildContext, Animation<double>, Animation<double>, Widget)?
      customTransitionBuilder,
}) {
  final ThemeData theme = Theme.of(context).copyWith(
    shadowColor: Colors.transparent,
  );
  return showGeneralDialog(
    context: context,
    barrierDismissible: barrierDismissible,
    barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
    barrierColor: Colors.black.withOpacity(0.01),
    pageBuilder: (context, a1, a2) => Theme(data: theme, child: child),
    transitionDuration: Duration(milliseconds: displaySpeed),
    // Credit Mayur (https://medium.com/flutter-community/how-to-animate-dialogs-in-flutter-here-is-answer-492ea3a7262f) for this solution
    transitionBuilder: customTransitionBuilder ??
        (context, a1, a2, widget) {
          return Opacity(
            opacity: a1.value,
            child: widget,
          );
        },
  );
}

PopupProps cagnPopupPropsHelper(
    BuildContext context, PopupProps props, PositionDetails details) {
  var pp = props;
  final sw = DeviceUtility.screenWidth - 10;
  final sh = DeviceUtility.screenHeight - 10;
  final hw = sw / 2;
  final hh = sh / 2;
  if (pp.size.height > sh) {
    pp = pp.copyWith(size: Size(pp.size.width, sh));
  }
  if (pp.size.width > sw) {
    pp = pp.copyWith(size: Size(sw, pp.size.height));
  }
  if ((details.position.dy + details.size.height + pp.offset.dy) > hh &&
      pp.pivot != null) {
    pp = pp.copyWith(
      alignment: pp.pivot,
      pivot: pp.alignment,
    );
  }
  if (pp.size.height >= sh) {
    pp = PopupProps(
      alignment: Alignment.center,
      offset: kDefaultDialogOffset,
      size: pp.size,
    );
  }
  return pp;
}

@immutable
class PopupProps {
  /// The alignment of the popup. When used with a pivot, the alignment is
  /// based on the [AlignmentGeometry] of the [PositionAwareWidget] used.
  /// If no pivot is provided, the alignment is based on the screen.
  final Alignment alignment;

  /// The area of the popup to use when aligning. Pivot is used in conjunction
  /// with alignment. If no pivot is provided, the alignment is based on the screen.
  final Alignment? pivot;

  /// The size of the popup widget. Defaults to 250 (w), 250 (h)
  final Size size;

  /// The [Offset] to use when positioning the popup. This offset acts as a margin
  /// around the popup. Defaults to zero.
  final Offset offset;

  const PopupProps({
    required this.alignment,
    this.size: kDefaultDialogSize,
    this.offset: kDefaultDialogOffset,
    this.pivot,
  });

  factory PopupProps.empty() => PopupProps(
        alignment: kDefaultDialogAlignment,
        pivot: kDefaultDialogPivot,
        size: kDefaultDialogSize,
        offset: kDefaultDialogOffset,
      );

  PopupProps copyWith({
    Alignment? alignment,
    Alignment? pivot,
    Size? size,
    Offset? offset,
  }) {
    return PopupProps(
      alignment: alignment ?? this.alignment,
      pivot: pivot ?? this.pivot,
      size: size ?? this.size,
      offset: offset ?? this.offset,
    );
  }
}
