import 'package:flutter/material.dart';

class PositionDetails {
  final Size size;
  final Offset position;

  const PositionDetails({this.size: Size.zero, this.position: Offset.zero});
}

/// Widget used to get information about where a user tapped on the screen.
/// The onTap behavior of this widget is similar to the [CupertinoButton].
class PositionAware extends StatefulWidget {
  PositionAware({
    Key? key,

    /// The widget to display.
    required this.child,

    /// The opacity to use when displaying the user interaction.
    this.activeOpacity: 0.7,

    /// The function to execute on user tap. Passes [PositionDetails]
    /// as a parameter.
    this.onTap,

    /// The function to execute on user long press. Passes [PositionDetails]
    /// as a parameter.
    this.onLongPress,

    /// The function to execute on user long press down. Passes [PositionDetails]
    /// as a parameter.
    this.onLongPressStart,

    /// The function to execute on user long press up. Passes [PositionDetails]
    /// as a parameter.
    this.onLongPressUp,
    this.behavior,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PositionAwareState();
  }

  final Widget child;
  final double activeOpacity;
  final Function(PositionDetails)? onTap;
  final Function(PositionDetails)? onLongPress;
  final Function(PositionDetails)? onLongPressUp;
  final Function(PositionDetails)? onLongPressStart;
  final HitTestBehavior? behavior;
}

class _PositionAwareState extends State<PositionAware>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  GlobalKey _key = GlobalKey();

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 100),
      lowerBound: widget.activeOpacity,
      upperBound: 1.0,
      value: 1.0,
    );
    _controller.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      key: _key,
      child: Opacity(
        child: widget.child,
        opacity: _controller.value,
      ),
      onTapDown: _onTapDown,
      onTapUp: _onTapUp,
      onTapCancel: _onTapCancel,
      onTap: _onTap,
      onLongPress: _onLongPress,
      onLongPressStart: _onLongPressStart,
      onLongPressUp: _onLongPressUp,
      behavior: widget.behavior,
    );
  }

  void _onTap() {
    if (widget.onTap != null)
      widget.onTap!(PositionDetails(
        size: _size,
        position: _offset,
      ));
  }

  void _onLongPress() {
    if (widget.onLongPress != null)
      widget.onLongPress!(PositionDetails(
        size: _size,
        position: _offset,
      ));
  }

  void _onLongPressStart(_) {
    if (widget.onLongPressStart != null)
      widget.onLongPressStart!(PositionDetails(
        size: _size,
        position: _offset,
      ));
  }

  void _onLongPressUp() {
    if (widget.onLongPressUp != null)
      widget.onLongPressUp!(PositionDetails(
        size: _size,
        position: _offset,
      ));
  }

  void _onTapDown(TapDownDetails details) {
    if (widget.activeOpacity != 1.0) {
      _controller.reverse();
    }
  }

  void _onTapUp(TapUpDetails details) {
    if (widget.activeOpacity != 1.0) {
      _controller.forward();
    }
  }

  void _onTapCancel() {
    if (widget.activeOpacity != 1.0) {
      _controller.forward();
    }
  }

  Size get _size {
    final keyContext = _key.currentContext;
    if (keyContext != null) {
      final box = keyContext.findRenderObject() as RenderBox;
      return box.size;
    }
    return Size.zero;
  }

  Offset get _offset {
    final keyContext = _key.currentContext;
    if (keyContext != null) {
      final box = keyContext.findRenderObject() as RenderBox;
      return box.localToGlobal(Offset.zero);
    }
    return Offset.zero;
  }
}
